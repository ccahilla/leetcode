'''
1.py
First problem on leetcode: https://leetcode.com/problems/two-sum/

Craig Cahillane
Nov 28, 2019
'''

from typing import List
class Solution:
    def twoSum(self, nums: List[int], target: int) -> List[int]:
        for idx1, num in enumerate(nums):
            target2 = target - num
            for idx2, num2 in enumerate(nums):
                if idx1 == idx2:
                    continue
                if num2 == target2:
                    idxs = [idx1, idx2]
                    return idxs
        return []


s = Solution()
nums = [2, 7, 11, 15] 
target = 22
idxs = s.twoSum(nums, target)
print(f'idxs = {idxs}')